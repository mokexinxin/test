package com.carpe.springboot.web;  

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.carpe.springboot.entity.Person;
import com.carpe.springboot.service.PersonService;
import com.carpe.springboot.webapi.ApiResult;
import com.carpe.springboot.webapi.ResultCode;

/**
 * 控制层
 * @author 李双
 *
 */

@RestController
public class PersonController {

	@Autowired
	private PersonService personService;
	
	/**
	 * 查找所有的person
	 * @return
	 */
	@RequestMapping("/person/findAllPerson")
	@ResponseBody
	ApiResult findAllPerson(){
		try {
			List<Person> list = personService.findAllPerson();
			return ApiResult.success(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiResult.failure(ResultCode.DATA_IS_WRONG);
	}
	
	/**
	 * 通过ID查找
	 * @param pId
	 * @return
	 */
	@RequestMapping("/person/findPerson/{pid}")
	@ResponseBody
	ApiResult findPerson(@PathVariable("pid") int pId){
		try {
			Person per = personService.findPersonById(pId);
			return ApiResult.success(per);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiResult.failure(ResultCode.DATA_IS_WRONG);
	}
	
	/**
	 * 更新人员信息
	 * @param person
	 * @return
	 */
	@RequestMapping("/person/update")
	@ResponseBody
	ApiResult update(Person person){
		try {
			int row = personService.update(person);
			return ApiResult.success(row);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiResult.failure(ResultCode.DATA_IS_WRONG);
	}
}
