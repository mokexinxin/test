package com.carpe.springboot.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.carpe.springboot.entity.Login;
import com.carpe.springboot.entity.Person;
import com.carpe.springboot.entity.Role;
import com.carpe.springboot.service.LoginService;
import com.carpe.springboot.service.RoleService;
import com.carpe.springboot.webapi.ApiResult;
import com.carpe.springboot.webapi.ResultCode;

/**
 * 用来控制登陆的Controller
 * @author 李双
 *
 */
@RestController
public class LoginController {
	@Autowired
	private LoginService loginService;

	@Autowired
	private RoleService roleService;

	/**
	 * 登陆
	 * @param user
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/login/findLoginByName")
	@ResponseBody
	public ApiResult findLoginByName(Login user, HttpServletRequest request) {
		Login login = null;
		login = loginService.findLoginByName(user);
		if (login != null) {
			Person person = new Person();
			person = login.getPerson();
			Role role = roleService.findRoleByPid(person.getPerson_id());
			if (role != null) {
				person.setRole(role);
			}
			login.setPerson(person);
			request.getSession().setAttribute("login", login);
			return ApiResult.success(login);
		} else {
			return ApiResult.failure(ResultCode.USER_LOGIN_ERROR);
		}
	}

}
