package com.carpe.springboot.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.carpe.springboot.entity.Customer;
import com.carpe.springboot.service.CustomerService;
import com.carpe.springboot.webapi.ApiResult;
import com.carpe.springboot.webapi.ResultCode;

/**
 * 客户控制类
 * @author 李双
 *
 */

@RestController
public class CustomerController {
	
	//自动注入客户业务类
	@Autowired
	private CustomerService customerService;
	
	
	/**
	 * 查找所有的客户
	 * @return
	 */
	@RequestMapping("/customer/findAllCus")
	@ResponseBody
	public ApiResult findAllCus() {
		List<Customer> allCus = customerService.findAllCus();
		return ApiResult.success(allCus);
	}
	
	
	/**
	 * 插入客户
	 * @param customer
	 * @return
	 */
	@RequestMapping("/customer/insert")
	@ResponseBody
	public ApiResult insert(Customer customer){
		try {
			
			int row = customerService.insert(customer);
			if(row==1){
				
				return ApiResult.success();
			}else{
				return ApiResult.failure(ResultCode.DATA_IS_WRONG);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiResult.failure(ResultCode.DATA_IS_WRONG);

	}
}
