package com.carpe.springboot.web;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.carpe.springboot.entity.Order;
import com.carpe.springboot.service.OrderService;
import com.carpe.springboot.webapi.ApiResult;
import com.carpe.springboot.webapi.ResultCode;
/**
 * 业务控制层
 * @author 李双
 *
 */
@RestController
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	/**
	 * 插入订单
	 * @param order实体类
	 * @return  处理结果的JSON数据
	 */
	@RequestMapping("/order/insert")
	@ResponseBody
	ApiResult insert(Order order){
		try {
			order.setOrder_time(new Date());
//			System.out.println(order);
			if(orderService.insert(order)){
				return ApiResult.success();
			}
		} catch (Exception e) {
			return ApiResult.failure(ResultCode.DATA_IS_WRONG);
		}
		return ApiResult.failure(ResultCode.DATA_IS_WRONG);
	}
	/**
	 * 查找所有订单
	 * @return
	 */
	@RequestMapping("/order/findAll")
	@ResponseBody
	ApiResult findAllOrder(){
		try {
			List<Order> list = orderService.findAllOrder();
			return ApiResult.success(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiResult.failure(ResultCode.DATA_IS_WRONG);
	}
	
	/**
	 * 根据id查找相应订单
	 * @param oId
	 * @return 订单实体
	 */
	@RequestMapping("/order/findById/{oId}")
	@ResponseBody
	ApiResult findOrderById(@PathVariable("oId") int oId){
		try {
			
			Order order = orderService.findOrderById(oId);
			return ApiResult.success(order);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiResult.failure(ResultCode.DATA_IS_WRONG);
	}
	
	/**
	 * 修改订单信息
	 * @param order
	 * @return 记录条数
	 */
	@RequestMapping("/order/update")
	@ResponseBody
	ApiResult update(Order order){
		try {
			if(orderService.update(order)){
				return ApiResult.success();
			}
		} catch (Exception e) {
			return ApiResult.failure(ResultCode.DATA_IS_WRONG);
		}
		return ApiResult.failure(ResultCode.DATA_IS_WRONG);
	}
	
	/**
	 * 删除订单
	 * @param oId
	 * @return
	 */
	@RequestMapping("/order/delete/{oId}")
	@ResponseBody
	ApiResult deleteById(@PathVariable("oId") int oId){
		try {
			if(orderService.deleteById(oId)){
				return ApiResult.success();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return ApiResult.failure(ResultCode.DATA_IS_WRONG);
	}
	
	@RequestMapping("/order/findOrderList")
	@ResponseBody
	ApiResult findOrderList(Order order,String order_time_start,String order_time_end){
		List<Order> findOrderList;
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("order_name", order.getOrder_name());
			map.put("order_type", order.getOrder_type());
			map.put("order_money", order.getOrder_money());
			map.put("order_num", order.getOrder_num());
			if(order_time_start != "" && order_time_start!=null){
				map.put("order_time_start",order_time_start);
			}
			if(order_time_end != "" && order_time_end!=null){
				map.put("order_time_end",order_time_end);
			}else{
				map.put("order_time_end",new Date());
			}
			findOrderList = orderService.findOrderList(map);
			return ApiResult.success(findOrderList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiResult.failure(ResultCode.DATA_IS_WRONG);
	}
}
