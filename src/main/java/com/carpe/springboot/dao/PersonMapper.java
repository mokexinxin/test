package com.carpe.springboot.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.carpe.springboot.entity.Person;

/**
 * 工作人员持久层
 * @author 李双
 *
 */
@Repository
public interface PersonMapper {
	/**
	 * 查找所有人员
	 * @return
	 */
	public List<Person> findAllPerson();
	
	/**
	 * 根据id查找人员
	 * @param pId
	 * @return
	 */
	public Person findPersonById(int pId);
	
	/**
	 * 修改人员信息
	 * @param person
	 * @return
	 */
	public int update(Person person);
	
	/**
	 * 删除人员信息
	 */
	public int deleteById(int id);
	
	/**
	 * 插入人员
	 */
	public int insert(Person person);
	
	/**
	 * 通过ID查询包含权限的人员信息
	 * @param pid
	 * @return
	 */
	Person findPersonByIdAndControl(int pid);
}
