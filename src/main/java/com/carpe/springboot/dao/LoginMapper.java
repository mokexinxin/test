package com.carpe.springboot.dao;

import org.springframework.stereotype.Repository;

import com.carpe.springboot.entity.Login;

/**
 * 登陆用户Mapper
 * @author 李双
 *
 */
@Repository
public interface LoginMapper {

	
	/**
	 * 查找登陆用户
	 * @return 登陆对象
	 */
	Login findLoginByName(Login login);
}
