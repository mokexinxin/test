package com.carpe.springboot.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
/**
 * 订单dao层
 * @author 李双
 *
 */

import com.carpe.springboot.entity.Order;
@Repository
public interface OrderMapper {
	/**
	 * 插入订单
	 * @param order
	 * @return 记录条数
	 */
	int insert(Order order);
	/**
	 * 查找所有订单
	 * @return
	 */
	List<Order> findAllOrder();
	/**
	 * 根据id查找相应订单
	 * @param oId
	 * @return 订单实体
	 */
	Order findOrderById(int oId);
	/**
	 * 修改订单信息
	 * @param order
	 * @return 记录条数
	 */
	int update(Order order);
	/**
	 * 删除订单
	 * @param oId
	 * @return
	 */
	int deleteById(int oId);
	
	/**
	 * 条件查找
	 * @param map
	 * @return
	 */
	List<Order> findOrderList(Map<String,Object> map);
}
