package com.carpe.springboot.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.carpe.springboot.entity.Customer;


/**
 * 客户持久层
 * @author 李双
 *
 */
@Repository
public interface CustomerMapper {

	/**
	 * 查询所有的客户信息
	 * @return  客户list集合
	 */
	List<Customer> findAllCus();
	/**
	 * 插入
	 * @return
	 */
	public int insert(Customer customer);
}
