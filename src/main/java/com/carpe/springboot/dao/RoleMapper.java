package com.carpe.springboot.dao;

import org.springframework.stereotype.Repository;

import com.carpe.springboot.entity.Role;

/**
 * 角色持久层
 * @author 李双
 *
 */
@Repository
public interface RoleMapper {
	
	Role findRoleById(int pid);

}
