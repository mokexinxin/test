package com.carpe.springboot.entity;

import java.util.List;


/**
 * 角色实体类
 * @author 李双
 *
 */
public class Role {
	private int role_id;
	private String role_name;
	private List<Control> controls;
	public Role() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Role(int role_id, String role_name) {
		super();
		this.role_id = role_id;
		this.role_name = role_name;
	}
	
	public Role(int role_id, String role_name, List<Control> controls) {
		super();
		this.role_id = role_id;
		this.role_name = role_name;
		this.controls = controls;
	}
	public int getRole_id() {
		return role_id;
	}
	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}
	public String getRole_name() {
		return role_name;
	}
	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}
	
	public List<Control> getControls() {
		return controls;
	}
	public void setControls(List<Control> controls) {
		this.controls = controls;
	}
	@Override
	public String toString() {
		return "Role [role_id=" + role_id + ", role_name=" + role_name + ", controls=" + controls + "]";
	}
}
