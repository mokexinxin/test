package com.carpe.springboot.entity;


/**
 * 权限实体
 * @author 李双
 *
 */
public class Control {
	private int control_id;
	private int control_code;
	private String control_name;
	public Control() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Control(int control_id, int control_code, String control_name) {
		super();
		this.control_id = control_id;
		this.control_code = control_code;
		this.control_name = control_name;
	}
	public int getControl_id() {
		return control_id;
	}
	public void setControl_id(int control_id) {
		this.control_id = control_id;
	}
	public int getControl_code() {
		return control_code;
	}
	public void setControl_code(int control_code) {
		this.control_code = control_code;
	}
	public String getControl_name() {
		return control_name;
	}
	public void setControl_name(String control_name) {
		this.control_name = control_name;
	}
	@Override
	public String toString() {
		return "Control [control_id=" + control_id + ", control_code="
				+ control_code + ", control_name=" + control_name + "]";
	}
}
