package com.carpe.springboot.entity;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 人员表
 * @author 李双
 *
 */
public class Person {
	private int person_id;
	private String person_name;
	private String person_phone;
	@DateTimeFormat(pattern="yyyy-mm-dd")
	private Date person_birthday;
	private String person_email;
	private int person_state;
	private Role role;
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Person(int person_id, String person_name, String person_phone,
			Date person_birthday, String person_email) {
		super();
		this.person_id = person_id;
		this.person_name = person_name;
		this.person_phone = person_phone;
		this.person_birthday = person_birthday;
		this.person_email = person_email;
	}
	
	public int getPerson_id() {
		return person_id;
	}
	public void setPerson_id(int person_id) {
		this.person_id = person_id;
	}
	public String getPerson_name() {
		return person_name;
	}
	public void setPerson_name(String person_name) {
		this.person_name = person_name;
	}
	public String getPerson_phone() {
		return person_phone;
	}
	public void setPerson_phone(String person_phone) {
		this.person_phone = person_phone;
	}
	@JsonFormat(pattern = "yyyy-mm-dd")
	public Date getPerson_birthday() {
		return person_birthday;
	}
	@JsonFormat(pattern = "yyyy-mm-dd")
	public void setPerson_birthday(Date person_birthday) {
		this.person_birthday = person_birthday;
	}
	public String getPerson_email() {
		return person_email;
	}
	public void setPerson_email(String person_email) {
		this.person_email = person_email;
	}
	public int getPerson_state() {
		return person_state;
	}
	public void setPerson_state(int person_state) {
		this.person_state = person_state;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "Person [person_id=" + person_id + ", person_name=" + person_name + ", person_phone=" + person_phone
				+ ", person_birthday=" + person_birthday + ", person_email=" + person_email + ", person_state="
				+ person_state + ", role=" + role + "]";
	}
}
