package com.carpe.springboot.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * 销售实体类
 * @author 李双
 *
 */
public class Order {
	private int order_id;
	private String order_num;
	private String order_name;
	private String order_type;
	private Double order_money;
	@DateTimeFormat(pattern = "yyyy-mm-dd HH:mm:ss")
	private Date order_time;
	private int person_id;
	
	public Order() {
	}

	

	public Order(int order_id, String order_num, String order_name, String order_type, Double order_money,
			Date order_time, int person_id) {
		super();
		this.order_id = order_id;
		this.order_num = order_num;
		this.order_name = order_name;
		this.order_type = order_type;
		this.order_money = order_money;
		this.order_time = order_time;
		this.person_id = person_id;
	}



	public int getOrder_id() {
		return order_id;
	}

	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}

	public String getOrder_num() {
		return order_num;
	}

	public void setOrder_num(String order_num) {
		this.order_num = order_num;
	}

	public String getOrder_name() {
		return order_name;
	}

	public void setOrder_name(String order_name) {
		this.order_name = order_name;
	}

	public String getOrder_type() {
		return order_type;
	}

	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}

	public Double getOrder_money() {
		return order_money;
	}

	public void setOrder_money(Double order_money) {
		this.order_money = order_money;
	}
	@JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss")
	public Date getOrder_time() {
		return order_time;
	}
	@JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss")
	public void setOrder_time(Date order_time) {
		this.order_time = order_time;
	}



	public int getPerson_id() {
		return person_id;
	}



	public void setPerson_id(int person_id) {
		this.person_id = person_id;
	}



	@Override
	public String toString() {
		return "Order [order_id=" + order_id + ", order_num=" + order_num + ", order_name=" + order_name
				+ ", order_type=" + order_type + ", order_money=" + order_money + ", order_time=" + order_time
				+ ", person_id=" + person_id + "]";
	}


	
	
}
