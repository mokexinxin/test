package com.carpe.springboot.entity;

/**
 * 登陆用户实体类
 * @author 李双
 *
 */
public class Login {
	
	private int login_id;
	
	private String login_name;
	
	private String login_pwd;
	
	private Person person;
	
	public Login() {
	}

	public Login(int login_id, String login_name, String login_pwd, Person person) {
		super();
		this.login_id = login_id;
		this.login_name = login_name;
		this.login_pwd = login_pwd;
		this.person = person;
	}

	public int getLogin_id() {
		return login_id;
	}

	public void setLogin_id(int login_id) {
		this.login_id = login_id;
	}

	public String getLogin_name() {
		return login_name;
	}

	public void setLogin_name(String login_name) {
		this.login_name = login_name;
	}

	public String getLogin_pwd() {
		return login_pwd;
	}

	public void setLogin_pwd(String login_pwd) {
		this.login_pwd = login_pwd;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "Login [login_id=" + login_id + ", login_name=" + login_name + ", login_pwd=" + login_pwd + ", person="
				+ person + "]";
	}
	
	

}
