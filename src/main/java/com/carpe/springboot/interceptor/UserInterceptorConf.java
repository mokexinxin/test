package com.carpe.springboot.interceptor;

import org.apache.catalina.startup.UserConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 拦截器配置类
 * 
 * @author 李双
 *
 */
@SpringBootConfiguration
public class UserInterceptorConf extends WebMvcConfigurerAdapter {

	@Autowired
	private UserInterceptor userInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(userInterceptor).addPathPatterns("/**").excludePathPatterns("/login/findLoginByName");
	}

}
