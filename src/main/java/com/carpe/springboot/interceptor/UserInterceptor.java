package com.carpe.springboot.interceptor;

import java.util.List;

import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.carpe.springboot.entity.Control;
import com.carpe.springboot.entity.Login;
import com.carpe.springboot.entity.Person;
import com.carpe.springboot.entity.Role;
import com.carpe.springboot.webapi.ApiResult;
import com.carpe.springboot.webapi.ResultCode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 全局过滤器，用来控制用户操作
 * @author 李双
 *
 */

@Component
@WebFilter(filterName="userInterceptor",urlPatterns="/*")
@Order(2)
public class UserInterceptor extends HandlerInterceptorAdapter{
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		//进行用户操作控制
		Login login = (Login) request.getSession().getAttribute("login");
		if(login!=null){
			if(login.getPerson()!=null){
				Person person = login.getPerson();
				Role role = person.getRole();
				List<Control> controls = role.getControls();
				//判断当前登陆用户的角色
				if(role.getRole_id()==1){
					String requestURI = request.getRequestURI();
//					System.out.println(requestURI);
					if(requestURI.equals("/order/insert") || requestURI.equals("/customer/insert")){
						return true;
					}else{
						ApiResult failure = ApiResult.failure(ResultCode.LACK_OF_AUTHORITY);
						ObjectMapper mapper = new ObjectMapper();
						String str = mapper.writeValueAsString(failure);
						response.getWriter().write(str);
						return false;
					}
				}
				return true;
			}
			return true;
		}else{
			//用户为登陆
			ApiResult failure = ApiResult.failure(ResultCode.USER_NOT_LOGGED_IN);
			ObjectMapper mapper = new ObjectMapper();
			String str = mapper.writeValueAsString(failure);
			response.getWriter().write(str);
			return false;
		}
	}
	
	

}
