package com.carpe.springboot.interceptor;

import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


/**
 * 全局过滤器,用来进行跨域操作
 * @author 李双
 *
 */

@Component
//制定要过滤的链接
@WebFilter(filterName="requestInterceor",urlPatterns="/*")
@Order(1)	//指定过滤器执行顺序
public class RequestInterceptor extends HandlerInterceptorAdapter{
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("Access-Control-Allow-Methods","POST,GET,PUT,DELETE");//,GET,PUT,DELETE
		response.setHeader("Access-Control-Allow-Headers","Access-Control,jwt,content-type");
		response.setHeader("Allow","POST");
		response.setCharacterEncoding("UTF-8");
		System.out.println("配置生效");
		return true;
	}

}
