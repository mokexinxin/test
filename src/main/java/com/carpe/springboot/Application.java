package com.carpe.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
@MapperScan("com.carpe.springboot.dao") //自动扫描tapper，如果这里不写就要在dao层加上mapper
public class Application {

	public static void main(String[] args) {
		//app启动类
		SpringApplication.run(Application.class, args);
	}
}
