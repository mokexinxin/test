package com.carpe.springboot.service;

import com.carpe.springboot.entity.Login;

/**
 * 登陆业务接口
 * @author 李双
 *
 */
public interface LoginService {

	
	/**
	 * 查找登陆用户
	 * @return 登陆对象
	 */
	Login findLoginByName(Login user);
}
