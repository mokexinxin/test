package com.carpe.springboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carpe.springboot.dao.LoginMapper;
import com.carpe.springboot.entity.Login;
import com.carpe.springboot.service.LoginService;

/**
 * 登陆业务实现类
 * @author 李双
 *
 */
@Service
public class LoginServiceImpl implements LoginService {
	
	@Autowired
	private LoginMapper loginMapper;
	
	/**
	 * 查找登陆用户
	 * @return 登陆对象
	 */
	public Login findLoginByName(Login user) {
		Login login = new Login();
		login = loginMapper.findLoginByName(user);
		return login;
	}

}
