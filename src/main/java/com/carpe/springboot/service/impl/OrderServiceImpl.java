package com.carpe.springboot.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.carpe.springboot.dao.OrderMapper;
import com.carpe.springboot.entity.Order;
import com.carpe.springboot.service.OrderService;

/**
 * 业务实现类
 * @author 李双
 *
 */
@Service
public class OrderServiceImpl implements OrderService {
	@Autowired
	private OrderMapper orderMapper;
	
	/**
	 * 插入订单
	 * @param order
	 * @return 记录条数
	 */
	public boolean insert(Order order) {
		return orderMapper.insert(order)==1?true:false;
	}
	/**
	 * 查找所有订单
	 * @return
	 */
	public List<Order> findAllOrder() {
		List<Order> list = orderMapper.findAllOrder();
		return list;
	}
	/**
	 * 根据id查找相应订单
	 * @param oId
	 * @return 订单实体
	 */
	public Order findOrderById(int oId) {
		
		return orderMapper.findOrderById(oId);
	}
	/**
	 * 修改订单信息
	 * @param order
	 * @return 记录条数
	 */
	public boolean update(Order order) {
		return orderMapper.update(order)==1?true:false;
	}
	/**
	 * 删除订单
	 * @param oId
	 * @return
	 */
	public boolean deleteById(int oId) {
		return orderMapper.deleteById(oId)==1?true:false;
	}
	
	/**
	 * 条件查找
	 * @param map
	 * @return
	 */
	public List<Order> findOrderList(Map<String,Object> map){
		if(map.get("order_time_end") == "" || map.get("order_time_end")==null){
			map.put("order_time_end", new Date());
		}
		return orderMapper.findOrderList(map);
	}

}
