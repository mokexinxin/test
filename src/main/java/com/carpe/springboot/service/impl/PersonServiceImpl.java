package com.carpe.springboot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.carpe.springboot.dao.PersonMapper;
import com.carpe.springboot.dao.RoleMapper;
import com.carpe.springboot.entity.Person;
import com.carpe.springboot.entity.Role;
import com.carpe.springboot.service.PersonService;

/**
 * 人员CURD业务实现类
 * @author 李双
 *
 */
@Service
@Repository
public class PersonServiceImpl implements PersonService{
	@Autowired
	private PersonMapper personMapper;
	
	@Autowired
	private RoleMapper roleMapper;
	/**
	 * 查找所有人员
	 * @return
	 */
	public List<Person> findAllPerson() {
		List<Person> list = personMapper.findAllPerson();
		return list;
	}
	
	/**
	 * 根据id查找人员
	 * @param pId
	 * @return
	 */
	public Person findPersonById(int pId) {
		Person per = null;
		per = personMapper.findPersonById(pId);
		return per;
	}

	/**
	 * 修改人员信息
	 */
	public int update(Person person) {
		int row = personMapper.update(person);
		return row;
	}
	/**
	 * 删除人员信息
	 * @param id
	 * @return
	 */
	public int deleteById(int id) {
		int row = personMapper.deleteById(id);
		return row;
	}
	/**
	 * 插入人员信息
	 */
	public int insert(Person person) {
		int row = personMapper.insert(person);
		return row;
	}
	
	/**
	 * 通过ID查询包含权限的人员信息
	 * @param pid
	 * @return
	 */
	public Person findPersonByIdAndControl(int pid){
		return personMapper.findPersonByIdAndControl(pid);
	}

}
