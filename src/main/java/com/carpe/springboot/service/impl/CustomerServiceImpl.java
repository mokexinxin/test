package com.carpe.springboot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carpe.springboot.dao.CustomerMapper;
import com.carpe.springboot.entity.Customer;
import com.carpe.springboot.service.CustomerService;


/**
 * 客户业务实现类
 * @author 李双
 *
 */
@Service
public class CustomerServiceImpl implements CustomerService{
	@Autowired
	private CustomerMapper customerMapper;
	
	/**
	 * 查询所有的客户信息
	 * @return  客户list集合
	 */
	public List<Customer> findAllCus() {
		return customerMapper.findAllCus();
	}

	public int insert(Customer customer) {
		
		return customerMapper.insert(customer);
	}

}
