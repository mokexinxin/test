package com.carpe.springboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carpe.springboot.dao.RoleMapper;
import com.carpe.springboot.entity.Role;
import com.carpe.springboot.service.RoleService;


/**
 * 角色实现类
 * @author 李双
 *
 */
@Service
public class RoleServiceImpl implements RoleService{

	@Autowired
	private RoleMapper roleMapper;
	
	/**
	 * 通过person_id查找角色信息
	 * @param pid
	 * @return
	 */
	public Role findRoleByPid(int pid){
		Role role = new Role();
		role = roleMapper.findRoleById(pid);
		return role;
	}
}
