package com.carpe.springboot.service;

import java.util.List;

import com.carpe.springboot.entity.Person;

/**
 * person的业务逻辑
 * @author 李双
 *
 */
public interface PersonService {
	
	/**
	 * 通过ID查询包含权限的人员信息
	 * @param pid
	 * @return
	 */
	Person findPersonByIdAndControl(int pid);
	
	/**
	 * 查找所有人员
	 * @return
	 */
	public List<Person> findAllPerson();
	
	/**
	 * 根据id查找人员
	 * @param pId
	 * @return
	 */
	public Person findPersonById(int pId);
	
	/**
	 * 修改人员信息
	 */
	public int update(Person person);
	/**
	 * 删除人员信息
	 * @param id
	 * @return
	 */
	public int deleteById(int id);
	/**
	 * 插入人员信息
	 * @param person
	 * @return
	 */
	public int insert(Person person); 
}

