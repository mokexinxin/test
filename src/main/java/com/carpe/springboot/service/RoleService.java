package com.carpe.springboot.service;

import com.carpe.springboot.entity.Role;

/**
 * 角色业务层
 * @author 李双
 *
 */
public interface RoleService {
	
	
	/**
	 * 通过person_id查找角色信息
	 * @param pid
	 * @return
	 */
	public Role findRoleByPid(int pid);

}
