package com.carpe.springboot.service;

import java.util.List;

import com.carpe.springboot.entity.Customer;


/**
 * 客户业务逻辑类接口
 * @author 李双
 *
 */
public interface CustomerService {

	/**
	 * 查询所有的客户信息
	 * @return  客户list集合
	 */
	List<Customer> findAllCus();
	
	int insert(Customer customer);
}
