# test

#### 项目介绍
用户报备系统，仅用于个人测试

#### 软件架构
使用 
    springboot框架
    TDD开发方式


#### 安装教程

1. 下载项目
2. 运行Application类即可(或将项目放进tomcat8的web服务器)
3.需要mysql的支持

#### 使用说明(使用80端口)localhost

1. 登陆                   /login/findLoginByName 
2. 插入销售订单           /order/insert
3. 查询所有订单           /order/findAll
4. 根据ID查找订单          /order/findById/{oId}
5. 修改订单信息            /order/update
6. 条件查找订单            /order/findOrderList
7. 查找所有的人员信息      /person/findAllPerson
8. 通过ID查找人员          /person/findPerson/{pid}
9. 修改人员信息            /person/update


#### 此项目仅为个人测试


